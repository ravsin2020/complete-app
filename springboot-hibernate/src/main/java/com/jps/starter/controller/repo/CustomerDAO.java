package com.jps.starter.controller.repo;

import java.util.List;

import com.jps.starter.bean.Customer;
public interface CustomerDAO {
	public List<Customer> getAllCustomer();

	public void createCustomer(Customer customer);
}

package com.jps.movieinfo.starter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jps.movieinfo.starter.bean.MovieInfo;
import com.jps.movieinfo.starter.service.MovieInfoService;

@RestController
@RequestMapping(path = "/movieinfo", produces = "application/json; charset=utf-8", consumes="application/json")
public class MovieInfoController {
	
	@Autowired
	MovieInfoService service;
	
	@RequestMapping(method = { RequestMethod.GET }, consumes="application/json")
	public ResponseEntity<List<MovieInfo>> getMoviesInfo(){
		List<MovieInfo> movies= service.getMoviesInfo();
		return ResponseEntity.ok(movies);
	}
	
	@RequestMapping(path="/{movieId}",method = { RequestMethod.GET }, consumes="application/json")
	public ResponseEntity<MovieInfo> getMovieInfoByMovieNAme(@PathVariable("movieId") Integer movieId ){
		MovieInfo movieInfo= service.getMovieInfoByMovieName(movieId);
		return ResponseEntity.ok(movieInfo);
	}
}

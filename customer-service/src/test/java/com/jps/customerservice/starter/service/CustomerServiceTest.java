package com.jps.customerservice.starter.service;

import static org.mockito.Mockito.when;

import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jps.customerservice.starter.beans.Customer;
import com.jps.customerservice.starter.dao.CustomerServiceDAO;
import com.jps.customerservice.starter.dao.CustomerServiceDAOImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerServiceTest{
	@Mock
	SessionFactory sessionFactory;
	
	@Mock
	CustomerServiceDAO customerDAO= new CustomerServiceDAOImpl();
	
	@InjectMocks
	CustomerService service= new CustomerServiceImpl();
	
	@Test
	public void getCustomerByIdTest(){
		when(customerDAO.getCustomerById(100)).thenReturn(getCustomerObject());
		Customer result = service.getCustomerById(100);
		Assert.assertEquals(100, result.getId());
	}
	private Customer getCustomerObject(){
		Customer cus= new Customer();
		cus.setId(100);
		cus.setName("Raj");
		cus.setAge(10);
		return cus;
		
	}
	
	
	/*@Autowired
	CustomerServiceDAO customerServiceDAO;
	
	@Override
	public List<Customer> getCustomers() {
		List<Customer> customers= customerServiceDAO.getCustomers();;		
		return customers;
	}

	@Override
	public Customer getCustomerById(Integer id) {
		
		return customerServiceDAO.getCustomerById(id);
	}*/

}

package com.jps.custprofile.starter.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jps.custprofile.starter.beans.Customer;
import com.jps.custprofile.starter.repo.CustomerDAO;
@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	CustomerDAO customerDAO;

	@Override
	public Map<Integer, Customer> getCustomers() {
		List<Customer> customers= customerDAO.getCustomers();;
		Map<Integer, Customer> map= new HashMap<Integer, Customer>();
		for (Customer customer : customers) {
			map.put(customer.getId(), customer);
		}
		
		return map;
	}

	@Override
	public Customer getCustomerById(Integer id) {
		
		return customerDAO.getCustomerById(id);
	}

	@Override
	public String  updateCustomer(Customer customer) {
		int retVal=customerDAO.updateCustomer(customer);
		return retVal==0?"Not updated" : "Customer Details updated Successfully !!";
	}

	@Override
	public String createCustomer(Customer customer) {
		int retVal = customerDAO.createCustomer(customer);
		return  retVal==0?"Not updated" : "Customer Details updated Successfully !!";
	}

	@Override
	public String deleteCustomer(Customer customer) {
		customer.setStatus("D");
		int retVal=customerDAO.updateCustomer(customer);
		return retVal==0?" Customer Not deleted" : "Customer deleted Successfully !!";
	}

}

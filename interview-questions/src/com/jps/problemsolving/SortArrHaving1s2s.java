package com.jps.problemsolving;

public class SortArrHaving1s2s {
public static void main(String[] args) {
	int[] arr={1,2,1,2,1,1,1,2,2,2,2,2,1,1};
	arr = sortArray(arr);
	for (int i = 0; i < arr.length; i++) {
		System.out.println(arr[i]);
	}
}
public static int[] sortArray(int[] arr){
	int countOf1s = 0;
	for (int i = 0; i < arr.length; i++) {
		if(arr[i]==1){
			countOf1s++;
		}
	} 
	for (int i = 0; i < arr.length; i++) {
		if(countOf1s>0){
			arr[i] = 1;
			countOf1s--;
			continue;
		}
		arr[i]=2;
		}
	return  arr;
}
}

package com.jps.test.openText;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class FindDuplicateProducts {
	public static void main(String[] args) {
		MyObject ball1 = new MyObject("ball", 1, 1);
		MyObject ball2 = new MyObject("ball", 1, 3);
		MyObject box1 = new MyObject("box", 2, 2);
		MyObject box2 = new MyObject("box", 1, 1);
		MyObject box3 = new MyObject("box", 1, 1);
		MyObject box4 = new MyObject("box", 1, 1);

		List<MyObject> objects = Arrays.asList(ball1, ball2, box1, box2, box3, box4);
		Map<MyObject, Object> map = new HashMap<>();
		Object v = new Object();
		int dup = 0;
		for (MyObject obj : objects) {
			Object o = map.get(obj);
			if (null == o) {
				map.put(obj, v);
			} else {
				dup++;
			}
		}
		System.out.println("Duplicate:" + dup);

	}
}

class MyObject {
	private String name;
	private int price;
	private int weight;

	public MyObject(String name, int price, int weight) {
		this.name = name;
		this.price = price;
		this.weight = weight;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof MyObject))
			return false;
		MyObject other = (MyObject) obj;
		return this.name.equalsIgnoreCase(other.name) && this.price == other.price && this.weight == other.weight;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, weight, price);
	}
}

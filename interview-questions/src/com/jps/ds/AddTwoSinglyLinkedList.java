package com.jps.ds;
/**
 * 
 * @author Ravikant.singh
 *
 * @param <T>
 * Given two number represented by two lists, write a function that returns the sum list. 
 * The sum list is list representation of the addition of two input numbers. 
 * Input: List1: 3->6->5  // represents number 365
       List2: 2->4->8 //  represents number 248
       Output: Resultant list: 6->1->3  // represents number 613
 */
public class AddTwoSinglyLinkedList<T> {
	public static void main(String[] args) {
		SinglyLinkedList<Integer> list1 = new SinglyLinkedList<>();
		list1.add(2);
		list1.add(3);
		list1.add(9);
		list1.add(5);
		list1.add(1);
		list1.add(2);
		SinglyLinkedList<Integer> list2 = new SinglyLinkedList<>();
		list2.add(7);
		list2.add(1);
		list2.add(2);
		AddTwoSinglyLinkedList<Integer> list = new AddTwoSinglyLinkedList<>();
		// SinglyLinkedList<Integer> result = list.addTwoList(list1, list2);
		// result.iterate();
		System.out.println("****************");

		SinglyLinkedList<Integer> list3 = new SinglyLinkedList<>();
		list3.add(9);
		list3.add(5);
		list3.add(7);
		SinglyLinkedList<Integer> list4 = new SinglyLinkedList<>();
		list4.add(7);
		list4.add(1);
		list4.add(2);
		SinglyLinkedList<Integer> result2 = list.addTwoList(list3, list4);
		result2.iterate();

	}

	public SinglyLinkedList<Integer> addTwoList(SinglyLinkedList<Integer> list1, SinglyLinkedList<Integer> list2) {
		if (list1 != null && list1 != null) {
			list1.reverse();
			list2.reverse();
			int qus = 0;
			Node<Integer> temp1 = list1.head;
			Node<Integer> temp2 = list2.head;
			SinglyLinkedList<Integer> list = new SinglyLinkedList<>();
			while (temp1 != null && temp2 != null) {
				int val = (temp1 == null ? 0 : temp1.data) + (temp2 == null ? 0 : temp2.data) + qus;
				qus = val >= 10 ? 1 : 0;
				val = val % 10;
				temp1 = (temp1 == null ? null : temp1.next);
				temp2 = (temp2 == null ? null : temp2.next);
				list.add(val);
			}
			if (qus != 0) {
				list.add(qus);
			}
			list.reverse();
			return list;
		}
		return ((list1 != null) ? list1 : list2);

	}

	private static class Node<T> {
		private T data;
		private Node<T> next;

		Node(T data) {
			this.data = data;
			this.next = null;
		}
	}

	private static class SinglyLinkedList<T> {
		private Node<T> head;

		public void add(T data) {
			Node<T> newNode = new Node<>(data);
			if (head == null) {
				head = newNode;
			} else {
				Node<T> temp = head;
				while (temp.next != null) {
					temp = temp.next;
				}
				temp.next = newNode;

			}
		}

		private void reverse() {
			Node<T> prev = null; Node<T> next; Node<T> current = head;
			while (current != null) {
				next = current.next;
				current.next = prev;
				prev = current;
				current = next;
			}
			head = prev;
		}

		public void iterate() {
			if (head != null) {
				Node<T> temp = head;
				while (temp != null) {
					System.out.println(temp.data);
					temp = temp.next;
				}
			}
		}

		SinglyLinkedList() {
			this.head = null;
		}
	}
}

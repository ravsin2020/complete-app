package com.jps.starter.security;

import com.jps.starter.bean.User;

public interface SecurityDAO {

	User getPasswordOfUser(String userName);

}

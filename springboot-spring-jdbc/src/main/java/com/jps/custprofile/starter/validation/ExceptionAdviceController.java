package com.jps.custprofile.starter.validation;

import java.sql.SQLException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionAdviceController {
	
	@ExceptionHandler(value=InvalidCustomerException.class)
	public ResponseEntity<Object> exception(InvalidCustomerException exception) {
		return new ResponseEntity<Object>(exception.getMsg(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value=SQLException.class)
	public ResponseEntity<Object> exception(SQLException exception) {
		return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value=Exception.class)
	public ResponseEntity<Object> exception(Exception exception) {
		return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.NOT_FOUND);
	}

}

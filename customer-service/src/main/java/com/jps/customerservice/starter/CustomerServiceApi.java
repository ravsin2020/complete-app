package com.jps.customerservice.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/*
 * added - exclude=HibernateJpaAutoConfiguration.class because of the below reasons
 * org.springframework.orm.jpa.EntityManagerHolder 
 * cannot be cast to org.springframework.orm.hibernate5.SessionHolder
 */
@SpringBootApplication(exclude=HibernateJpaAutoConfiguration.class)
@EnableEurekaClient
public class CustomerServiceApi {
public static void main(String[] args) {
	SpringApplication.run(CustomerServiceApi.class, args);
}
}

package com.jps.starter.security;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.jps.starter.bean.User;

@Service
@Transactional
public class SecurityServiceImpl implements SecurityService{
	@Autowired
	SecurityDAO securityDao;
	@Override
	public User getPasswordOfUser(String userName) {
		return securityDao.getPasswordOfUser(userName);
	}

}

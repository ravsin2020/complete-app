package com.jps.problemsolving;

import java.util.TreeSet;

/**
 * 
 * @author Ravikant.singh
 *
 */
public class RemoveDupSortInDec {
public static void main(String[] args) {
	
	//Use TreeSet to store the Object. This will remove duplicate and 
	// based on sorting algo it will do the sorting as well
	TreeSet<Student> list= new TreeSet<>();
	list.add(new Student(100, "Ravi", 2299, 25));
	list.add(new Student(100, "Ravi", 2299, 25));
	list.add(new Student(101, "Shyam", 2300, 72));
	list.add(new Student(102, "Ritu", 9999, 30));
	list.add(new Student(103, "Ravi Singh", 4001, 92));
	list.forEach(System.out::println);
	
}
}
/**
 * 
 * @author Ravikant.singh
 * Write a class which implements comparable interface,
 * override compareTo method and write sorting algorithms based on age
 */
class Student implements Comparable<Student>{
	private Integer id;
	private String name;
	private Integer rollNo;
	private Integer age;
	@Override
	public int compareTo(Student stu0) {
		return stu0.getAge().compareTo(this.age) ;
	}
	public Student(Integer id, String name, Integer rollNo, Integer age) {
		super();
		this.id = id;
		this.name = name;
		this.rollNo = rollNo;
		this.age = age;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRollNo() {
		return rollNo;
	}
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((rollNo == null) ? 0 : rollNo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rollNo == null) {
			if (other.rollNo != null)
				return false;
		} else if (!rollNo.equals(other.rollNo))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", rollNo=" + rollNo + ", age=" + age + "]";
	}
}

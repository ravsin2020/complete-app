package com.jps.multithreading;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
/**
 * 
 * @author Ravikant.singh
 *  create 3 thread and print the below value range and finally do sum
	1st Thread - 1-500
	2nd Thread - 1-1000
	3rd - 1-1500
 * 
 *
 */
public class Create3ThreadsProcessSomeTaskParallel {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService service = Executors.newFixedThreadPool(3);
		List<Integer> list = new ArrayList<>();
		int start = 1;
		int end = 0;
		for (int i = 0; i < 3; i++) {
			end = end + 500;
			Future<Integer> val = service.submit(new Processor(start, end));
			list.add(val.get());
		}
		service.shutdown();
		try {
			int finalSum = list.stream().mapToInt(e -> e.intValue()).sum();
			System.out.println(finalSum);
		} catch (Exception e2) {
		}

	}
}

class Processor implements Callable<Integer> {
	private int start;
	private int end;

	Processor(int start, int end) {
		this.start = start;
		this.end = end;
	}

	@Override
	public Integer call() throws Exception {
		int sum = 0;
		for (int i = start; i < end; i++) {
			sum += i;
		}
		return sum;
	}
}

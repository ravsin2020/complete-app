package com.jps.customerservice.starter.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jps.customerservice.starter.beans.Customer;
@Repository
public class CustomerServiceDAOImpl implements CustomerServiceDAO {
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<Customer> getCustomers() {
		Session session= sessionFactory.getCurrentSession();
		Criteria c= session.createCriteria(Customer.class);
		List<Customer> customers= c.list();
		return customers;
	}

	@Override
	public Customer getCustomerById(Integer id) {
		Session session= sessionFactory.getCurrentSession();
		Customer cust= session.get(Customer.class, id);
		return cust;
	}

}

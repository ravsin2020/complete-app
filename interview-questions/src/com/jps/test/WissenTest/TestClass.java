package com.jps.test.WissenTest;

import java.io.*;
import java.util.*;
/**
 * 
 * @author Ravi
 *
Input
4
5,10 0,20 25,40 35,45
Your Code's Output
2
Expected Correct Output
2
 
 *
 *
 */
public class TestClass {
	
	static int minParkingSpaces(int[][] parkingStartEndTimes) {
		Map<Integer, Integer> maps = new TreeMap<>();
		Set<Integer> startTimes = new TreeSet<>();
		for (int i = 0; i < parkingStartEndTimes.length; i++) {
			int[] temp = new int[2];
			for (int j = 0; j < 2; j++) {
				temp[j] = parkingStartEndTimes[i][j];
			}
			maps.put(temp[0], temp[1]);
			startTimes.add(temp[0]);
		}
		int maxSlots = 0;
		boolean timeUpForDay;
		for (Integer startTime : startTimes) {
			timeUpForDay = false;
			int tempMaxSlot = 0;
			Integer nextParkingStartTime = startTime;
			int currParkingEndTime = maps.get(startTime);
			while (!timeUpForDay) {
				tempMaxSlot++;
				nextParkingStartTime = maps.get(currParkingEndTime) != null ? maps.get(currParkingEndTime)
						: getNextParkingStartTime(startTimes, currParkingEndTime);
				timeUpForDay = (nextParkingStartTime == null || nextParkingStartTime == 0) ? true : false;
				currParkingEndTime = maps.get(nextParkingStartTime);
			}
			if (tempMaxSlot > maxSlots) {
				maxSlots = tempMaxSlot;
			}
		}
		return maxSlots;

	}
	public static int getNextParkingStartTime(Set<Integer> timings, int preEnd){
		int nextTime=0;
		for (Integer nextStart : timings) {
			if(nextStart>preEnd){
				nextTime= nextStart;
				break;
			}
		}
		return nextTime;
	}

	// DO NOT MODIFY ANYTHING BELOW THIS LINE!!

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter wr = new PrintWriter(System.out);
		int n = Integer.parseInt(br.readLine().trim());
		int[][] parkingStartEndTimeList = new int[n][2];
		String[] parkingStartEndTimes = br.readLine().split(" ");
		for (int i = 0; i < n; i++) {
			String[] parkingStartEndTime = parkingStartEndTimes[i].split(",");
			for (int j = 0; j < parkingStartEndTime.length; j++) {
				parkingStartEndTimeList[i][j] = Integer.parseInt(parkingStartEndTime[j]);
			}
		}

		int out = minParkingSpaces(parkingStartEndTimeList);
		System.out.println(out);

		wr.close();
		br.close();
	}
}
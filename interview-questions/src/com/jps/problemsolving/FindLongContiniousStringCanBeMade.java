package com.jps.problemsolving;
/**
 * 
 * @author Ravikant.Singh
 * ex - input - abxyzlmcdefn
 * o/p- abcdef
 *
 */
public class FindLongContiniousStringCanBeMade {
	public static void main(String[] args) {
		System.out.println("Longest length : " + findLongestString("abxyzlmcdefn"));
	}
	public static String findLongestString(String str){
		String val=""; String temp="";
		int[] arr= new int[256]; // initializing array with full keyboard numbers 
		for(int i=0; i<str.length();i++){
			arr[str.charAt(i)]=1; // if char found then marking them as 1
		}
		for(int i=64;i<122;i++){
			if(arr[i]==0){
				temp ="";
			}else{
				
				temp=temp+ (char)i;
			}
			if(val.length()<temp.length()){
				val=temp;
			}
		}
		return val;
	}
}

package com.jps.ds.basic;
/**
 * 
 * @author Ravikant.Singh
 * Custom Singly Linked List
 *
 * @param <T>
 */
public class SinglyLinkedList<T> {
	private Node<T> head;

	public SinglyLinkedList() {
		head = null;
	}

	public void add(T val) {
		Node<T> node = new Node<>(val);
		if (head == null) {
			head = node;
		} else {
			Node<T> temp = head;
			while (temp.getNext() != null) {
				temp = temp.getNext();
			}
			temp.setNext(node);
		}
	}

	public void iterate() {
		Node<T> temp = head;
		while (temp != null) {
			System.out.println(temp.getData());
			temp = temp.getNext();
		}
	}

	public void removeLast() {
		Node<T> temp = head;
		if (temp == null) {
			return;
		}
		if (temp.getNext() != null) {
			while (temp.getNext().getNext() != null) {
				temp = temp.getNext();
			}
			temp.setNext(null);
		} else if (temp.getNext() == null) {
			head = null;
		} else {
			temp.setNext(null);
		}
	}

	public void removeFirst() {
		Node<T> temp = head;
		if (temp == null) {
			return;
		}
		temp = head.getNext();
		head = temp;
	}

	public Node<T> getHead() {
		return head;
	}

	public void setHead(Node<T> head) {
		this.head = head;
	}
	
}
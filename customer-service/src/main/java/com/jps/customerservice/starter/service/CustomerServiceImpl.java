package com.jps.customerservice.starter.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jps.customerservice.starter.beans.Customer;
import com.jps.customerservice.starter.dao.CustomerServiceDAO;
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	CustomerServiceDAO customerServiceDAO;
	
	@Override
	public List<Customer> getCustomers() {
		List<Customer> customers= customerServiceDAO.getCustomers();;		
		return customers;
	}

	@Override
	public Customer getCustomerById(Integer id) {
		
		return customerServiceDAO.getCustomerById(id);
	}

}

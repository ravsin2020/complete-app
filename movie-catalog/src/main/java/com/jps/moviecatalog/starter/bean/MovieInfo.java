package com.jps.moviecatalog.starter.bean;

import java.io.Serializable;

import org.springframework.stereotype.Component;
@Component
public class MovieInfo implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 6957494636235444434L;

private int id;
private String name;
private String actor;
private double rating;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getActor() {
	return actor;
}
public void setActor(String actor) {
	this.actor = actor;
}
public double getRating() {
	return rating;
}
public void setRating(double rating) {
	this.rating = rating;
}

}

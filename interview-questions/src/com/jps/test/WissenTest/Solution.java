package com.jps.test.WissenTest;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * 
 * @author Ravi
 * input-
1
Virat_Kohli 22,Steve_Smith 33,Virat_Kohli 45,Steve_Smith 55,Virat_Kohli 66

Expected Correct Output
Steve_Smith 44.0000 2
Virat_Kohli 44.3333 3
 *
 */
public class Solution {
    public static class PlayerStatisticsCollectorImpl implements PlayerStatisticsCollector {
        Map<String,double[]> playerList= new HashMap<>();
        @Override
        public void putNewInnings(String player, int runs){
            if(playerList.containsKey(player)){
                double[] det=playerList.get(player);
                det[0]=det[0] + runs;
                det[1] = det[1]+ 1;
                playerList.put(player,det);
            }else{
                double[] det= new double[2];
                det[0]=runs; det[1]=1;
                playerList.put(player,det);
            }
        }

        @Override
        public double getAverageRuns(String player){
        	double[] det=playerList.get(player);
        	return det[0]/det[1];
        }

        @Override
        public int getInningsCount(String player){
        	double[] det=playerList.get(player);
        	return (int)det[1];
        }
    }

    ////////////////// DO NOT MODIFY BELOW THIS LINE ///////////////////

    public interface PlayerStatisticsCollector {
        // This is an input. Make note of this player inning.  Runs is a non negative integer.
        void putNewInnings(String player, int runs);

        // Get the runs average(mathematical average) for a player
        double getAverageRuns(String player);

        // Get the total number of innings for the player
        int getInningsCount(String player);
    }

    public static void main(String[] args) {
Scanner scanner = new Scanner(System.in);
        int numLines = Integer.parseInt(scanner.nextLine());
        int currentLine = 0;
        while (currentLine++ < numLines) {
            final PlayerStatisticsCollector stats = new PlayerStatisticsCollectorImpl();
            final Set<String> players = new TreeSet<>();

            String line = scanner.nextLine();
            String[] inputs = line.split(",");
            for (int i = 0; i < inputs.length; ++i) {
                String[] tokens = inputs[i].split(" ");
                final String player = tokens[0];
                players.add(player);
                final int runs = Integer.parseInt(tokens[1]);

                stats.putNewInnings(player, runs);

            }

            for (String player : players) {
                System.out.println(
                        String.format("%s %.4f %d", player, stats.getAverageRuns(player), stats.getInningsCount(player)));
            }

        }
        scanner.close();

    }
}

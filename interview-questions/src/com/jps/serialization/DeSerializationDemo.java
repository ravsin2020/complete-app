package com.jps.serialization;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

public class DeSerializationDemo {
	public static void main(String[] args) {
		try {
			
			FileInputStream fis= new FileInputStream("C:/Users/Ravi/Desktop/File IO,Serialization, Excaption Handling & Logging/source/SerDemo.text");
			ObjectInputStream ois = new ObjectInputStream(fis);
			User user= (User)ois.readObject();
			System.out.println(user.toString());
			fis.close();
			ois.close();
			
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}

package com.jps.moviecatalog.starter.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.jps.moviecatalog.starter.bean.Customer;
import com.jps.moviecatalog.starter.bean.MovieCatalog;
import com.jps.moviecatalog.starter.bean.MovieInfo;
import com.jps.moviecatalog.starter.bean.Ticket;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Component
public class MovieCatalogServiceImpl {

	@Autowired
	RestTemplate restTemplate;

	public List<MovieCatalog> getMoviesCatalog() {
		List<MovieCatalog> catList = new ArrayList<MovieCatalog>();
		/*ResponseEntity<Customer[]> response = restTemplate.getForEntity("http://localhost:8788/customers",
				Customer[].class);
		Customer[] employees = response.getBody();
		List<Customer> customers = Arrays.asList(employees);
		customers.stream().forEach(e -> {
			MovieCatalog cat = new MovieCatalog();
			cat.setCustomer(e);
			catList.add(cat);
		});*/
		return catList;
	}
	
	private HttpEntity getHttpEntityRequest(){
		/*HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));*/
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity request = new HttpEntity(headers);
		return request;
	}
	@HystrixCommand(fallbackMethod="getMovieCataLog")
	public MovieCatalog getMovieCataLogByCustomerId(Integer custId) {
		MovieCatalog cat = new MovieCatalog();
		HttpEntity request= getHttpEntityRequest();
		// make an HTTP GET request with headers
		/*ResponseEntity<Customer> response = restTemplate.exchange("http://localhost:8788/customers/" + custId, HttpMethod.GET,
				request, Customer.class);*/
		ResponseEntity<Customer> response = restTemplate.exchange("http://"+"CUSTOMER-SERVICE" +"/customers/" + custId, HttpMethod.GET,
				request, Customer.class);

		// Get the Customer Details
		Customer customer = (Customer) response.getBody();
		cat.setCustomer(customer);

		// Get the Ticket List
		ResponseEntity<?> tickResponse = restTemplate.exchange("http://"+"TICKET-BOOKING-SERVICE"+ "/tickets/"+custId, HttpMethod.GET,
				request, Ticket[].class);

		Ticket[] tickets = (Ticket[]) tickResponse.getBody();
		if (tickets != null && tickets.length > 0) {
			// get the movie Details
			List<MovieInfo> movies = new ArrayList<>();
			ResponseEntity<?> movResponse = null;
			for (int i = 0; i < tickets.length; i++) {
				// ResponseEntity<Customer> response =
				movResponse = restTemplate.exchange("http://"+"MOVIE-INFO-SERVICE"+ "/movieinfo/"+tickets[i].getMovieId(), HttpMethod.GET, request,
						MovieInfo.class);

				MovieInfo movieInfo = (MovieInfo) movResponse.getBody();
				movies.add(movieInfo);
			}

			cat.setMovies(movies);
		}
		return cat;
	}
	public MovieCatalog getMovieCataLog(Integer custId){
		return new MovieCatalog();
	}

}

package com.jps.customerservice.starter.service;

import java.util.List;

import com.jps.customerservice.starter.beans.Customer;


public interface CustomerService {
		public List<Customer> getCustomers();
		public Customer getCustomerById(Integer id);
}

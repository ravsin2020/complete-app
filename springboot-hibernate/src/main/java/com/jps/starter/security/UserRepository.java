package com.jps.starter.security;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.jps.starter.bean.User;

public interface UserRepository extends CrudRepository<User, Long> {

   @Query("FROM user_credential u WHERE u.username = :username")
   public User getUserByUsername(@Param("username") String username);
}
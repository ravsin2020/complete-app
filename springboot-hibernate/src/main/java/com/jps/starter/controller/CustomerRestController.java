package com.jps.starter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jps.starter.bean.Customer;
import com.jps.starter.controller.service.CustomerService;

@RestController
@RequestMapping(path ="/customers")
public class CustomerRestController {
	@Autowired
	CustomerService customerService;
	
	@RequestMapping(path="/hello", method = { RequestMethod.GET })
	public String sayHello() {
		return "Hello";
	}

	@RequestMapping(method = { RequestMethod.GET })
	public List<Customer> getAllCustomer() {
		List<Customer> custList = customerService.getAllCustomer();
		return custList;
	}
	@RequestMapping(method = { RequestMethod.POST })
	public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
		customerService.createCustomer(customer);
		return ResponseEntity.ok(customer);
	}
}

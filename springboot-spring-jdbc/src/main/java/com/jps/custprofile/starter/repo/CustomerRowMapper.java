package com.jps.custprofile.starter.repo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.jps.custprofile.starter.beans.Customer;

public class CustomerRowMapper implements RowMapper<Customer> {

	@Override
	public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Customer custmoer = new Customer();
		custmoer.setName(rs.getString("name"));
		custmoer.setId(rs.getInt("id"));
		custmoer.setAge(rs.getInt("age"));
		custmoer.setStatus(rs.getString("status"));
		return custmoer;
	}

}

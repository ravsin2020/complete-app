package com.jps.ticketbooking.starter.service;

import java.util.List;

import com.jps.ticketbooking.starter.bean.Ticket;

public interface TicketBookingService {

	public List<Ticket> getTickets();
	public List<Ticket> getTicketsByCustomerId(Integer custId);

}

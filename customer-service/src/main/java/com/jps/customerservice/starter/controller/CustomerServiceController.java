package com.jps.customerservice.starter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jps.customerservice.starter.beans.Customer;
import com.jps.customerservice.starter.service.CustomerService;

@RestController
@RequestMapping(path = "/customers", produces = "application/json; charset=utf-8")
public class CustomerServiceController {
	@Autowired
	CustomerService service;
	
	@RequestMapping(method = { RequestMethod.GET })
	public ResponseEntity<List<Customer>> getCustomers() {
		List<Customer> result = service.getCustomers();
		return ResponseEntity.ok(result);
		
	}

	@RequestMapping(path = "/{id}", method = { RequestMethod.GET })
	public ResponseEntity<Customer> getCustomerDetails(@PathVariable("id") Integer id) {
		Customer cust=null;
		cust= service.getCustomerById(id);
		return ResponseEntity.ok(cust);
	}
}

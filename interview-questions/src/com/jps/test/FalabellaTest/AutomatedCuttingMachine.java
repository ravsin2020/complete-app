package com.jps.test.FalabellaTest;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Ravi
 *
 */
public class AutomatedCuttingMachine {
	public static void main(String[] args) {
		 int[] lengths = {3,4,5,2};//{4,3,4,5,3}; - 9 //{4, 3, 2};- 7
		 List<Integer> list= new ArrayList<>();
		 list.add(3);
		 list.add(4);
		 list.add(5);
		 list.add(2);
	     System.out.println(isRodCutPossible(list, 9, 0, list.size()-1));
	     System.out.println(FindSolution(lengths, 7));
	}
	
	public static String FindSolution(int[] arr, int minLength ){
		return arr[0]+arr[1] >= minLength || arr[1] +arr[2] >=minLength ? "Possible" : "Impossible";
	}

    public static boolean isRodCutPossible(List<Integer> lengths, int min, int start, int end) {
        if (start >= lengths.size() && end >= lengths.size()) {
            return false;
        }
        if (sum(lengths, start, end) >= min && end - start < lengths.size()) {
            return true;
        }

        boolean possible = false;
        if (end >= lengths.size()) {
            possible |= isRodCutPossible(lengths, min, start + 1, start + 1);
        }
        else {
            possible |= isRodCutPossible(lengths, min, start, end + 1);
        }
        return possible;
    }

    private static int sum(List<Integer> lengths, int start, int end) {
        System.out.println(start + "-" + end);
        Integer sum = 0;
        for (int i = start; i < end; i++) {
            sum = sum +lengths.get(i);
        }
        return sum;
    }
}

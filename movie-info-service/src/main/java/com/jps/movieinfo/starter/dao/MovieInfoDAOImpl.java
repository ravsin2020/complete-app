package com.jps.movieinfo.starter.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jps.movieinfo.starter.bean.MovieInfo;
@Repository
public class MovieInfoDAOImpl implements MovieInfoDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List<MovieInfo> getMoviesInfo() {
		List<MovieInfo> moviesInfo = sessionFactory.getCurrentSession().createCriteria(MovieInfo.class).list();
		return null;
	}

	@Override
	public MovieInfo getMovieInfoByMovieName(Integer movieId) {
		MovieInfo info = (MovieInfo) (sessionFactory.getCurrentSession().createQuery("from movieinfo where id=:id")
				.setParameter("id", movieId).list() == null
				|| sessionFactory.getCurrentSession().createQuery("from movieinfo where id=:id")
						.setParameter("id", movieId).list().isEmpty() ? null
								: sessionFactory.getCurrentSession().createQuery("from movieinfo where id=:id")
										.setParameter("id", movieId).list().get(0));
		return info;
	}

}

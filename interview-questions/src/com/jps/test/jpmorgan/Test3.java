package com.jps.test.jpmorgan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Ravi Word Search Programming challenge description: Write a program
 *         to find all words in a string that satisfy the following conditions:
 * 
 *         Contains characters a - z, A - Z, digits 0 - 9, or underscore Starts
 *         with a capital letter A - Z Has at least one digit 0 - 9 Any
 *         character that does not fit the first condition can be used to
 *         separate distinct words.
 * 
 *         Use regular expression(s) to search.
 * 
 *         Input: A string in which to search. For example:
 * 
 *         Theory Lab_99 society Imagi4nation Output: Print out the list of
 *         found words in order of their appearance in the input string.
 * 
 *         Print each word on a separate line. For example:
 * 
 *         Lab_99 Imagi4nation Print NONE if there are no words in the string,
 *         matching necessary rules.
 * 
 *         Test 1 Test Input Download 
 *         Test 1 
 *         Input - Theory Lab_99 society Imagi4nation 
 *         Expected Output Download Test 1 Input 
 *         Lab_99 
 *         Imagi4nation 
 *         
 *         Test 2 Test Input Download Test 2 
 *         Input - a1 B2 Cd xy_3 KL0 hw-57 
 *         Expected Output Download Test 2 Input 
 *         B2 
 *         KL0
 *         
 *         Test 3 Test Input Download Test 3 
 *         Input - passion Safety 5_Responsibility 
 *         Expected
 *         Output Download Test 3 Input 
 *         NONE 
 *         
 *         Test 4 Test Input Download Test 4
 *         Input - Soft2uare presentation Proposal_13 love complaint height freedom 784 Message0 organization Se55ion 
 *         Expected Output Download Test 4 Input 
 *         Soft2uare 
 *         Proposal_13 
 *         Message0 Se55ion
 *
 */
public class Test3 {
	/**
	 * Iterate through each line of input.
	 */
	public static void main(String[] args) throws IOException {

		InputStreamReader reader = new InputStreamReader(System.in, StandardCharsets.UTF_8);
		BufferedReader in = new BufferedReader(reader);
		String line;
		boolean found = false;
		boolean isMatch = false;
		Pattern pattern = Pattern.compile("^(?=.*[A-Z])(?=.*\\d).+$");
		while ((line = in.readLine()) != null) {
			// System.out.println(line);
			String[] strArr = line.split(" ");
			for (int i = 0; i < strArr.length; i++) {
				Matcher matcher = pattern.matcher(strArr[i]);
				isMatch = matcher.matches();
				if (isMatch && Character.isUpperCase(strArr[i].charAt(0))) {
					found = true;
					System.out.println(strArr[i]);
				}
			}
			if (!found) {
				System.out.println("NONE");
			}
		}

	}
}
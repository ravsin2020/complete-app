package com.jps.moviecatalog.starter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jps.moviecatalog.starter.bean.MovieCatalog;
import com.jps.moviecatalog.starter.service.MovieCatalogServiceImpl;

@RestController
@RequestMapping(path = "/movie-catalog/catalog",produces = "application/json; charset=utf-8", consumes=MediaType.APPLICATION_JSON_VALUE)
@EnableCircuitBreaker
public class MovieCatalogController {
	
	@Autowired
	MovieCatalogServiceImpl service;
	
	@RequestMapping(method = { RequestMethod.GET })
	public List<MovieCatalog> getMovieCatalog(){
		return service.getMoviesCatalog();
	}
	
	@RequestMapping(path="{custId}",method = { RequestMethod.GET })
	public ResponseEntity<MovieCatalog> MovieCatalogService(@PathVariable("custId") Integer custId ){
		MovieCatalog cat = service.getMovieCataLogByCustomerId(custId);
		return ResponseEntity.ok(cat);
	}

	public MovieCatalogServiceImpl getService() {
		return service;
	}

	public void setService(MovieCatalogServiceImpl service) {
		this.service = service;
	}
	

}

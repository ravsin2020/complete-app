package com.jps.test.FalabellaTest;

public class StringReduction {
	public static void main(String[] args) {
		System.out.println("abab : " + findNoOfDeductionNeeded("abab"));
		System.out.println("abcab : " + findNoOfDeductionNeeded("abcabc"));
	}

	public static int findNoOfDeductionNeeded(String str) {
		int result = 0;
		boolean[] isAvailable = new boolean[122];
		for (int i = 0; i < str.length(); i++) {
			if (isAvailable[str.charAt(i)]) {
				result++;
			} else {
				isAvailable[str.charAt(i)] = true;
			}
		}
		return result;
	}
}

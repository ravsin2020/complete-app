package com.jps.serialization;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Customer extends Person implements Externalizable {
	private int id;
	private transient String name;
	static String city;
	private int accountNo;
	private Address address;
	/**
	 * Default constructor is needed to do default initialization 
	 * of the properties which is not being serialize
	 */
	public Customer(){
		System.out.println("Customer()");
	}
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(this.id);
		out.writeObject(this.name);
		out.writeObject(Customer.city);
		out.writeObject(this.address);
	}
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.id = in.readInt();
		this.name = (String) in.readObject();
		Customer.city=(String) in.readObject();
		this.address = (Address)in.readObject();
	}
	public Customer(int id, String name, String city, int accountNo, Address address) {
		super();
		this.id = id;
		this.name = name;
		Customer.city= city;
		this.accountNo = accountNo;
		this.address= address;
		System.out.println("Customer(int id, String name, String city, int accountNo)");
	}
	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", city=" + city +", accountNo=" + accountNo + " ,address=" +address+"]";
	}
	

}

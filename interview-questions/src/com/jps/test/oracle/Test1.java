package com.jps.test.oracle;

import java.util.Arrays;

public class Test1 {
public static void main(String[] args) {
	System.out.println("Is Anagram : " + checkAnagram("a bc", "bcA"));
}
public static boolean checkAnagram(String str1, String str2){
	if(str1==null || str2==null || str1.isEmpty() || str2.isEmpty()){
		return false;
	}
	str1 = str1.replaceAll("\\s", ""); str2 = str2.replaceAll("\\s", "");
	if(str1.length()!=str2.length()){
		return false;
	}
	char[] arr=new char[str1.length()]; 
	char[] arr2= new char[str2.length()];
    for(int i=0; i<str1.length(); i++){
    		arr[i]=Character.toLowerCase(str1.charAt(i));
    }
    for(int i=0; i<str2.length(); i++){
    		arr2[i]=Character.toLowerCase(str2.charAt(i)); 
    }
   
    Arrays.sort(arr); Arrays.sort(arr2);
    boolean isAnagram=true;
    for(int i=0;i<arr.length;i++){
    	if(arr[i]!=arr2[i]){
    		isAnagram  = false;
    		break;
    	}
    }
    return isAnagram;
}
}

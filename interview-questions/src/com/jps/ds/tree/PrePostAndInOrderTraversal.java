package com.jps.ds.tree;

public class PrePostAndInOrderTraversal {
	public static void main(String[] args) {
		Node treeHead=new Node(10);
		treeHead.setLeft(new Node(4));
		treeHead.setRight(new Node(7));
		treeHead.getLeft().setLeft(new Node(2));
		treeHead.getLeft().setRight(new Node(5));
		treeHead.getRight().setLeft(new Node(6));
		treeHead.getRight().setRight(new Node(8));
		System.out.println("**Pre Order Traversal**");
		preOrder(treeHead);
		System.out.println("**Post Order Traversal**");
		postOrder(treeHead);
		System.out.println("**In Order Traversal**");
		inOrder(treeHead);
		
		
	}
	public static void inOrder(Node root){
		if(root==null){
			return;
		}
		inOrder(root.getLeft());
		System.out.println(root.getData());
		inOrder(root.getRight());
	}
	public static void postOrder(Node root){
		if(root==null){
			return;
		}
		postOrder(root.getLeft());
		postOrder(root.getRight());
		System.out.println(root.getData());
	}
	public static void preOrder(Node root){
		if(root==null){
			return;
		}
		System.out.println(root.getData());
		preOrder(root.getLeft());
		preOrder(root.getRight());
	}

	}

class Node{
	private int data;
	private Node left,right;
	Node(int data){
		this.data=data;
		this.left=null;
		this.right=null;
	}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	public Node getLeft() {
		return left;
	}
	public void setLeft(Node left) {
		this.left = left;
	}
	public Node getRight() {
		return right;
	}
	public void setRight(Node right) {
		this.right = right;
	}
	
}

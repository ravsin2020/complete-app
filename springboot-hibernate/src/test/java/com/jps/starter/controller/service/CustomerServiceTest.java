package com.jps.starter.controller.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jps.starter.bean.Customer;
import com.jps.starter.controller.repo.CustomerDAO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerServiceTest implements CustomerService {
	
	@Mock
	CustomerDAO customerDAO;
	
	@InjectMocks
	CustomerService service = new CustomerServiceImpl();
	
	@BeforeEach
    void setMockOutput() {
        when(customerDAO.getAllCustomer()).thenReturn(getAllCustomer());
    }
	public List<Customer> getCustomers(){
		List<Customer> customers= new ArrayList<Customer>();
		Customer customer= new Customer();
		customer.setId(100);
		customer.setCustomerName("Ravi");
		customers.add(customer);
		return customers;
	}

    @DisplayName("Test Mock CustomerService + CustomerServiceDAO")
    @Test
    void testGet() {
    	List<Customer> result = service.getAllCustomer();
        assertEquals(100, result.get(0).getId());
        assertEquals("Ravi", result.get(0).getCustomerName());
    }
	@Override
	public List<Customer> getAllCustomer() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void createCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
	}

}

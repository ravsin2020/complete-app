package com.jps.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableSwagger2
public class SpringBootApi {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootApi.class, args);
	}
}

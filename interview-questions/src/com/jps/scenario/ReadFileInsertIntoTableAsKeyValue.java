package com.jps.scenario;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadFileInsertIntoTableAsKeyValue {
	public static void main(String[] args) throws IOException {
		//ExcelReaderUtil util= new ExcelReaderUtil();
		//ExcelReaderUtil util = new ExcelReaderUtil("C:/Users/Ravi/Ravikant/Interview resumes/2020 Interview.xlsx");
		//System.out.println(util.getCellData("Jagan", "Name", 1));
		 FileInputStream fis = null;
		 XSSFWorkbook workBook = null;
		 XSSFSheet sheet = null;
		fis = new FileInputStream("C:/Users/Ravi/Ravikant/Interview resumes/2020 Interview.xlsx");
		workBook = new XSSFWorkbook(fis);
	}

}

class ExcelReaderUtil {
	ExcelReaderUtil(){
		
	}
	private FileInputStream fis = null;
	private XSSFWorkbook workBook = null;
	private XSSFSheet sheet = null;
	private XSSFRow row = null;
	private XSSFCell cell = null;
	private String filePath = null;

	ExcelReaderUtil(String filePath) {
		this.filePath = filePath;
		try {
			this.fis = new FileInputStream(this.filePath);
			this.workBook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String getCellData(String sheetName, String colName, int rowNum) {
		try {
			int colNum = -1;
			sheet = workBook.getSheet("sheetName");
			row = sheet.getRow(0);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				if (row.getCell(i).getStringCellValue().trim().equals(colName)) {
					colNum = i;
				}
			}
			row = sheet.getRow(rowNum - 1);
			cell = row.getCell(colNum);
			return cell.getStringCellValue();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getCellHeaderData(String sheetName, String colName, int rowNum) {
		try {
			int colNum = -1;
			sheet = workBook.getSheet("sheetName");
			row = sheet.getRow(0);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				if (row.getCell(i).getStringCellValue().trim().equals(colName)) {
					colNum = i;
				}
			}
			row = sheet.getRow(rowNum - 1);
			cell = row.getCell(colNum);
			return cell.getStringCellValue();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

package com.jps.custprofile.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerProfileApi {
public static void main(String[] args) {
	SpringApplication.run(CustomerProfileApi.class, args);
}
}

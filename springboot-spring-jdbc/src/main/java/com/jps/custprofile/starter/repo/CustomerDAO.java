package com.jps.custprofile.starter.repo;

import java.util.List;

import com.jps.custprofile.starter.beans.Customer;

public interface CustomerDAO {
	public List<Customer> getCustomers();
	
	public Customer getCustomerById(Integer id);
	public int updateCustomer(Customer customer);
	public int createCustomer(Customer customer);

}

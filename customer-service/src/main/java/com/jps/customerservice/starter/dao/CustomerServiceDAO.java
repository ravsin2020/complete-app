package com.jps.customerservice.starter.dao;

import java.util.List;

import com.jps.customerservice.starter.beans.Customer;

public interface CustomerServiceDAO {
	public List<Customer> getCustomers();
	public Customer getCustomerById(Integer id);
}

package com.jps.starter.security;

import org.springframework.stereotype.Service;

import com.jps.starter.bean.User;
@Service
public interface SecurityService {
public User getPasswordOfUser(String userName);
}

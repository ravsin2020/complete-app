package com.jps.test.FalabellaTest;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MapOperation { // o/p- {a=1, b=2}
	public static void main(String[] args) {
		Map<String,Integer> map= new HashMap<>();
		map.put("a", 1);
		Collections.unmodifiableMap(map);
		map.put("b", 2);
		System.out.println(map);
	}
}

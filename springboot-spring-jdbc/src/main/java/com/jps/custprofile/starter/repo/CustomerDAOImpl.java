package com.jps.custprofile.starter.repo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.jps.custprofile.starter.beans.Customer;
import com.jps.custprofile.starter.validation.InvalidCustomerException;

@Repository
public class CustomerDAOImpl implements CustomerDAO{
	@Autowired
	JdbcTemplate  jdbcTemplate;

	@Override
	public List<Customer> getCustomers() {
				
		return jdbcTemplate.query("select * from customers where status='A'", new CustomerRowMapper());
	}
	
	@Override
	public Customer getCustomerById(Integer id) {
		Customer customer=null;
		try {
			customer = jdbcTemplate.queryForObject("select * from customers where id=?", new Object[] {
	                id
	            },
	            new BeanPropertyRowMapper < Customer > (Customer.class));
		} catch (Exception e) {
				throw new InvalidCustomerException("Invalid Customer : " + id);
		}
		return customer;
	}
	@Override
	public int updateCustomer(Customer customer) {
		return jdbcTemplate.update("update customers " + " set name = ?, age = ?, status = ? " + " where id = ?",
	            new Object[] {
	            		customer.getName(), customer.getAge(), customer.getStatus(), customer.getId()
	            });
	}

	@Override
	public int createCustomer(Customer customer) {
		 int result = jdbcTemplate.update("insert into customers (id, name, age, status) " + "values(?, ?, ?, ?)",
		            new Object[] {
		            		customer.getId(), customer.getName(), customer.getAge(), customer.getStatus()
		            });
		return result;
	}


	
	
}

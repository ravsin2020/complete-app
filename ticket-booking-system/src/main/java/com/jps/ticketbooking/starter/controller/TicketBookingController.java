package com.jps.ticketbooking.starter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jps.ticketbooking.starter.bean.Ticket;
import com.jps.ticketbooking.starter.service.TicketBookingService;

@RestController
@RequestMapping(path = "/tickets", produces = "application/json; charset=utf-8", consumes=MediaType.APPLICATION_JSON_VALUE)
public class TicketBookingController {
	
	@Autowired
	TicketBookingService service;
	
	@RequestMapping(method = { RequestMethod.GET }, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Ticket>> getTickets(){
		List<Ticket> tickets = service.getTickets();
		return ResponseEntity.ok(tickets);
	}
	
	@RequestMapping(path="/{custId}",method = { RequestMethod.GET },consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Ticket>> getTicketsByCustomerId(@PathVariable("custId") Integer custId ){
		List<Ticket> tickets =service.getTicketsByCustomerId(custId);
		return ResponseEntity.ok(tickets); 
	}

}

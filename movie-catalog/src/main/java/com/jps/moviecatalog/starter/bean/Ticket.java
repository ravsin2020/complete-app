package com.jps.moviecatalog.starter.bean;

import java.io.Serializable;

import org.springframework.stereotype.Component;
@Component
public class Ticket implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7635751965060344802L;
	private int id;
	private int custId;
	private int movieId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	
	

}

package com.jps.serialization;

import java.io.Serializable;

public class Person {
	public int uid;
	public String name;
	public Address permAddress;
	public Person(){
		System.out.println("Person()");
	}
	public Person(int uid, String name, Address permAddress) {
		super();
		this.uid = uid;
		this.name = name;
		this.permAddress = permAddress;
		System.out.println("Person(int uid, String name, Address permAddress)");
	}
	@Override
	public String toString() {
		return "Person [uid=" + uid + ", name=" + name + ", permAddress=" + permAddress + "]";
	}
}
class Employee extends Person implements Serializable{

	public int eid;
	public transient String cname;
	public Address compAddress;
	public static String account;
	public Employee(){
		System.out.println("Employee()");
	}
	public Employee(int eid, String cname, Address compAddress,String account) {
		super();
		this.eid = eid;
		this.cname = cname;
		this.compAddress = compAddress;
		Employee.account= account;
		System.out.println("Employee(int eid, String cname, Address compAddress, String account)");
	}
	@Override
	public String toString() {
		return "Employee [eid=" + eid + ", cname=" + cname + ", compAddress=" + compAddress + ", uid=" + uid + ", name="
				+ name + ", permAddress=" + permAddress + "]";
	}
		
}
class User extends Employee{
	private int userId; private String ss;
	public User(int userId) {
		super();
		this.userId = userId;
		System.out.println("User(int userId)");
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", eid=" + eid + ", cname=" + cname + ", compAddress=" + compAddress
				+ ", uid=" + uid + ", name=" + name + ", permAddress=" + permAddress + "]";
	}

	
}

class Address implements Serializable {
	private String city;
	private int pin;
	public Address(){
		System.out.println("Address()");
	}
	public Address(String city, int pin) {
		super();
		this.city = city;
		this.pin = pin;
		System.out.println("Address(String city, int pin)");
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	@Override
	public String toString() {
		return "Address [city=" + city + ", pin=" + pin + "]";
	}

}

package com.jps.exception;

public class CustomCheckedExceptionTest {
public static void main(String[] args){
	//Employee emp= new Employee(12, "ravi");
}
}

class Employee{
	private int id;
	private String name;
	Employee(int id,String name) throws NameValidationException{
		this.name= name;
		this.id=id;
		if(name==null || !containsAllAlphabet(name)){
			throw new NameValidationException("Invalid Name : name shoud contain vlue between a to z only");
		}
		
	}
	public boolean containsAllAlphabet(String name){
		for(int i=0;i<name.length();i++){
			char ch= name.charAt(i);
			if(ch<'a' || ch>'z'){
				return false;
			}
		}
		return true;
	}
	
}
class NameValidationException extends Exception{
	private String msg;
	NameValidationException(String msg){
		this.msg=msg;
	}
	@Override
	public String toString() {
		return "NameValidationException [msg=" + msg + "]";
	}
	
}



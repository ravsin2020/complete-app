package com.jps.moviecatalog.starter.bean;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5770120101450979693L;

	private Integer id;

	private String name;

	private int age;

	private String email;
	private String status;
	public Customer() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}

package com.jps.problemsolving;
/**
 * 
 * @author Ravikant.Singh
 * 
 * Reverse words in given String
 * for Ex- I am Ravi
 * o/p- Ravi am I
 *
 */
public class ReverseWordsInGivenString {
	public static void main(String[] args) {
		String str= reverse("I am Ravi");
		System.out.println(str);
		
	}
	public static String reverse(String str){
		if(str==null){
			return str;
		}
		String[] strArr=str.split(" "); int end=strArr.length-1;
		for (int i = 0; i < strArr.length/2; i++) {
			String temp=strArr[i]; 
			strArr[i]=strArr[end];
			strArr[end]=temp;
			end--;
		}
		StringBuffer result= new StringBuffer(strArr[0]);
		for (int i = 1; i < strArr.length; i++) {
			result.append(" " + strArr[i]);
		}
		return result.toString();
	}
}

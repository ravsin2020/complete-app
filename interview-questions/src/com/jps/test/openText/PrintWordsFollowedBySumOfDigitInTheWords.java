package com.jps.test.openText;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
/**
 * 
 * @author Ravi
 * input - ta1s2te the th34under
 * o/p- ta1s2te (3)
 * the (0)
 *th34under(7)
 *
 */
public class PrintWordsFollowedBySumOfDigitInTheWords {
    public static void main(String args[] ) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
         InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(reader);
        String line;
        Map<String, Integer> map = new LinkedHashMap<>();
        while ((line = in.readLine()) != null) {
            String[] strArr=line.split(" ");
             boolean isPresent=false;
            for(int k=0; k<strArr.length;k++){
                int val= findSumOfIntVal(strArr[k]);
                if(val>0){
                    isPresent=true;
                }
                map.put(strArr[k],   val );
            }
            if(isPresent){
                for (Map.Entry<String,Integer> vals : map.entrySet()) {
                    if(vals.getKey().contains("t")){
                    	  System.out.println(vals.getKey() + " (" +vals.getValue()+")");
                    }
                }
            }else{
                System.out.println("None");
            }
        }
    }
    public static int findSumOfIntVal(String str){
        if(str==null || str.isEmpty()){
            return 0;
        }
         int sum=0;
        for(int i=0;i<str.length();i++){
           if(Character.isDigit(str.charAt(i))){
               sum= sum+Character.getNumericValue(str.charAt(i));
           }
        }
         return sum;
    }
}
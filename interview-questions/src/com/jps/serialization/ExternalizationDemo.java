package com.jps.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ExternalizationDemo {
	public static void main(String[] args) {
		doSeralize();
		
		doDeseralize();
	}
	public static void doDeseralize(){
		try {
			FileInputStream fis= new FileInputStream("C:/Users/Ravi/Desktop/File IO,Serialization, Excaption Handling & Logging/source/ExtDemo.text");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Customer cust= (Customer)ois.readObject();
			
			System.out.println(" Deseralize Done");
			System.out.println(cust.toString());
			System.out.println(cust.uid);
			fis.close();
			ois.close();
			
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public static void doSeralize(){
		Address add= new Address("bangalore" , 560087);
		Customer cust = new Customer(1001, "Raj", "Bangalore", 927827, add);
		cust.uid=89887;
		try {
			FileOutputStream fos= new FileOutputStream("C:/Users/Ravi/Desktop/File IO,Serialization, Excaption Handling & Logging/source/ExtDemo.text");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(cust);
			fos.close();
			oos.close();
			System.out.println(" Seralize Done !!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

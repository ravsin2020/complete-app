package com.jps.movieinfo.starter.dao;

import java.util.List;

import com.jps.movieinfo.starter.bean.MovieInfo;

public interface MovieInfoDAO {

	List<MovieInfo> getMoviesInfo();

	MovieInfo getMovieInfoByMovieName(Integer movieId);

}

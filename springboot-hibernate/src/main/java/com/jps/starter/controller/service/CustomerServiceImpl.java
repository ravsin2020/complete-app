package com.jps.starter.controller.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jps.starter.bean.Customer;
import com.jps.starter.controller.repo.CustomerDAO;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	CustomerDAO customerDAO;

	public List<Customer> getAllCustomer() {
		return customerDAO.getAllCustomer();
	}

	@Override
	public void createCustomer(Customer customer) {
		customerDAO.createCustomer(customer);
		
	}

}

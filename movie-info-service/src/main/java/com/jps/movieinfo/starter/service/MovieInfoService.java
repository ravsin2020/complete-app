package com.jps.movieinfo.starter.service;

import java.util.List;

import com.jps.movieinfo.starter.bean.MovieInfo;

public interface MovieInfoService {

	List<MovieInfo> getMoviesInfo();

	MovieInfo getMovieInfoByMovieName(Integer movieId);

}

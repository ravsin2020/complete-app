package com.jps.starter.security;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jps.starter.bean.User;
@Repository
public class SecurityDAOImpl implements SecurityDAO {
	@Autowired
	private SessionFactory sessionFactory;
 
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	@Override
	public User getPasswordOfUser(String userName) {
		//Query query= sessionFactory.getCurrentSession().
		//        createQuery("from user_credential where userName=:userName");
		Query query= sessionFactory.getCurrentSession().getNamedQuery("findUserByName");
		query.setParameter("username", userName);
		User  user = (User) (query.list() ==null || query.list().isEmpty() ? null : query.list().get(0));
		return user;
	}

}

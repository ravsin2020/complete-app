package com.jps.problemsolving;

/**
 * 
 * @author Raviksint.singh Input: 
 * input arr[] = {4, 3, 7, 8, 6, 2, 1} 
 * Output: arr[] =
 *         {3, 7, 4, 8, 2, 6, 1}
 *
 */
public class ConvertArrayIntoZigZagFashion {
	public static void main(String[] args) {
		int[] arr= {4, 3, 7, 7, 6, 2, 1};
		arr = reArrangeArray(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		
	}
	public static int[] reArrangeArray(int[] arr){
		if(arr==null || arr.length<3){
			return arr;
		}
		boolean flag=true;
		for (int i = 0; i < arr.length-2; i++) {
			if(flag){
				if(arr[i] > arr[i+1]){ // arr[i] expected less
					//swap them
					arr[i+1]=arr[i]+arr[i+1];
					arr[i]=arr[i+1]-arr[i];
					arr[i+1]=arr[i+1]-arr[i];
				}
			}else{
				if(arr[i]<arr[i+1]){ // arr[i] expected more
					//swap them
					arr[i+1]=arr[i]+arr[i+1];
					arr[i]=arr[i+1]-arr[i];
					arr[i+1]=arr[i+1]-arr[i];
				}
			}
			flag=!flag;
		}
		return arr;
	}
}

package com.jps.customerservice.starter.controller;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.jps.demo.Addition;

public class AdditionTest {

	static Addition addService;

	@BeforeClass
	public static void init() {
		addService = new Addition();
	}

	@Test
	public void doDumTest1() {
		int result = addService.doAdd(5, 6);
		Assert.assertEquals(11, result);
	}
	
	@Test
	public void doDumTest2() {
		int result = addService.doAdd(-1, 6);
		Assert.assertEquals(5, result);
	}
	
	@AfterClass
	public static void destroy(){
		addService=null;
	}
	
	
}

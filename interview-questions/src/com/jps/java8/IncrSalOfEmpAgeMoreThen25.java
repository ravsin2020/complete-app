package com.jps.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
/**
 * 
 *  @author Ravikant.singh
 *  Given a list of Employee Object
 *  Increase the salary of employee the employee whose age is greater then 25
 *  find the sum of salary of those employee whose salary has been increased 
 *
 */
public class IncrSalOfEmpAgeMoreThen25 {
public static void main(String[] args) {
	
	List<String> list= new ArrayList<>();
	list.add("Raj");list.add("shyam"); list.add("ABV"); list.add("ss");
	List ll= list.stream().filter(c -> c.startsWith("s")).collect(Collectors.toList());
	ll.forEach(e->System.out.println(e));
	
	List<Employee> empList = new ArrayList<>();
	BiFunction<Integer, Double, Employee>  f= (age,salary)-> new Employee(age,salary);
	empList.add(f.apply(10, 1000.5));
	empList.add(f.apply(30, 2000.5));
	empList.add(f.apply(40, 3000.5));
	BiConsumer<Employee,Double> c= (e,increment)->{e.setSalary(e.getSalary()+100);};
	empList.stream().filter(e->e.getAge()>25).collect(Collectors.toList()).forEach(e->c.accept(e, 100.2));
	empList.forEach(e->System.out.println(e));
	System.out.println("***");
	Double totalSal= empList.stream().filter(e->e.getAge()>25).collect(Collectors.summingDouble(Employee::getSalary));
	System.out.println(totalSal);
	System.out.println("***************Other Way**************************");
	final List<Employee> emList= new ArrayList<>();
	emList.add(new Employee(29, 500.3));
	emList.add(new Employee(20, 100.2));
	emList.add(new Employee(26, 300.2));
	emList.stream().filter(x->x.getAge()>25).forEach(s->s.setSalary(s.getSalary()+100));
	System.out.println(emList.stream().filter(x->x.getAge()>25).mapToDouble(i->i.getSalary()).sum());
}
}
class Employee{
	private int age;
	private double salary;

	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public Employee(int age, double salary) {
		super();
		this.age = age;
		this.salary = salary;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		long temp;
		temp = Double.doubleToLongBits(salary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (age != other.age)
			return false;

		if (Double.doubleToLongBits(salary) != Double.doubleToLongBits(other.salary))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Employee [age=" + age + ", salary=" + salary + "]";
	}
	
	
	
}

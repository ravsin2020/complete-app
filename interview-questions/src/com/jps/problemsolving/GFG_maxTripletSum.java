package com.jps.problemsolving;

import java.util.Arrays;

public class GFG_maxTripletSum { 
	 // Driver code 
    public static void main(String args[]) 
    { 
        int arr[] = {5,2,8,5,1,5};//{ 2, 5, 3, 1, 4, 9 }; 
        int n = arr.length; 
        System.out.println(maxTripletSum(arr, n)); 
        System.out.println(maxTripletSum2(arr, n));
    } 
  
    // Function to calculate maximum triplet sum 
    static int maxTripletSum(int arr[], int n) 
    { 
        // Initialize the answer 
        int ans = 0; 
  
        for (int i = 1; i < n - 1; ++i) { 
            int max1 = 0, max2 = 0; 
  
            // find maximum value(less than arr[i]) 
            // from i+1 to n-1 
            for (int j = 0; j < i; ++j) 
                if (arr[j] < arr[i]) 
                    max1 = Math.max(max1, arr[j]); 
  
            // find maximum value(greater than arr[i]) 
            // from i+1 to n-1 
            for (int j = i + 1; j < n; ++j) 
                if (arr[j] > arr[i]) 
                    max2 = Math.max(max2, arr[j]); 
  
            // store maximum answer 
        if(max1 < max2) 
            ans = Math.max(ans, max1 + arr[i] + max2); 
        } 
  
        return ans; 
    } 
    static int maxTripletSum2(int arr[], int n) 
    { 
        // sort the given array 
        Arrays.sort(arr); 
      
        // After sorting the array.  
        // Add last three element  
        // of the given array 
        return arr[n - 1] + arr[n - 2] + arr[n - 3]; 
    }  
      
}
package com.jps.custprofile.starter.service;

import java.util.List;
import java.util.Map;

import com.jps.custprofile.starter.beans.Customer;

public interface CustomerService {
	public Map<Integer, Customer> getCustomers();
	public Customer getCustomerById(Integer id);
	public String  updateCustomer(Customer customer);
	public String createCustomer(Customer customer);
	public String deleteCustomer(Customer customer);

}

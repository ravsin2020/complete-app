package com.jps.ds;

import java.util.HashSet;
import java.util.Set;

import com.jps.ds.basic.Node;
import com.jps.ds.basic.SinglyLinkedList;

/**
 * 
 * @author Ravikant.singh There are two singly linked lists in a system. By some
 *         programming error, the end node of one of the linked list got linked
 *         to the second list,forming an inverted Y shaped list. Write a program
 *         to get the point where two linked list merge. 
 *         ex: 
 *         8->9->10-> 
 *         			 13-> 15->30 ->50
 *             2->5->
 *         intersect pint is 15
 *
 */
public class FindIntersectPointOfTwoSinglyLinkedList {
	public static void main(String[] args) {
		SinglyLinkedList<Integer> list1= new SinglyLinkedList<>();
		list1.setHead(new Node<>(8));
		list1.getHead().setNext(new Node<>(9));
		list1.getHead().getNext().setNext(new Node<>(10));
		list1.getHead().getNext().getNext().setNext(new Node<>(13));
		list1.getHead().getNext().getNext().getNext().setNext(new Node<>(15));
		list1.getHead().getNext().getNext().getNext().getNext().setNext(new Node<>(30));
		list1.getHead().getNext().getNext().getNext().getNext().getNext().setNext(new Node<>(50));
		SinglyLinkedList<Integer> list2= new SinglyLinkedList<>();
		list2.setHead(new Node<>(2));
		list2.getHead().setNext(new Node<>(5));
		list2.getHead().getNext().setNext(list1.getHead().getNext().getNext().getNext());
		
		findIntersectPoint(list1, list2);
		System.out.println("findIntersectPointUsingHasing");
		findIntersectPointUsingHasing(list1, list2);
	}

	/**
	 * 1.find the length of both the linked List 2.take the difference of length
	 * 3.start the largest linked list fist till the difference 3.start both
	 * large one(form difference point) and small one from start point till they
	 * meet to one point 4. meet point will be intersect point
	 * 
	 * @return int
	 */
	public static void findIntersectPoint(SinglyLinkedList<Integer> list1, SinglyLinkedList<Integer> list2) {
		if (list1 == null || list1.getHead() == null || list2 == null || list2.getHead() == null) {
			System.out.println("No intersect point");
			return;
		}
		int diff = 0;
		Node<Integer> node1 = list1.getHead();
		Node<Integer> node2 = list2.getHead();
		int l1= findLength(node1); 
		int l2= findLength(node2);
		if (l1 > l2) {
			diff = l1 - l2;
			while (diff > 0) {
				node1 = node1.getNext();
				diff--;
			}
		} else {
			diff = l2 - l1;
			while (diff > 0) {
				node2 = node2.getNext();
				diff--;
			}
		}
		while (node1 != null) {
			if (node1 == node2) {
				System.out.println("Intersect point : " + node1.getData());
				break;
			}
			node1 = node1.getNext();
			node2 = node2.getNext();
		}

	}
	public static int findLength(Node head){
		Node node= head;
		int length=0;
		while(node!=null){
			node=node.getNext();
			length++;
		}
		return length;
	}
	/**
	 * find intersect point using hashing.
	 * 1.Add one list to HashSet
	 * 2.add 2nd list to HashSet but while adding check whether it exist in set or not
	 * @param list1
	 * @param list2
	 */
	public static void findIntersectPointUsingHasing(SinglyLinkedList<Integer> list1, SinglyLinkedList<Integer> list2){
		if(list1==null || list1.getHead()==null || list2==null || list2.getHead()==null){
			System.out.println("No intersect point");
		}
		Set set= new HashSet();
		Node node1= list1.getHead(); Node node2= list2.getHead();
		while(node1!=null){
			set.add(node1);
			node1= node1.getNext();
		}
		while(node2!=null){
			if(!set.add(node2)){
				System.out.println("Intersect point is : " + node2.getData());
				break;
			}
			node2=node2.getNext();
		}
		
	}
}

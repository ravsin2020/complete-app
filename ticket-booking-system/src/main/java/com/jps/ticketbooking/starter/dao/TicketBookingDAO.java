package com.jps.ticketbooking.starter.dao;

import java.util.List;

import com.jps.ticketbooking.starter.bean.Ticket;

public interface TicketBookingDAO {

	List<Ticket> getTickets();

	List<Ticket> getTicketsByCustomerId(Integer custId);

}

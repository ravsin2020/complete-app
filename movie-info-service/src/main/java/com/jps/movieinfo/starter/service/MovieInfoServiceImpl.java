package com.jps.movieinfo.starter.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jps.movieinfo.starter.bean.MovieInfo;
import com.jps.movieinfo.starter.dao.MovieInfoDAO;
@Service
@Transactional
public class MovieInfoServiceImpl implements MovieInfoService {

	@Autowired
	MovieInfoDAO dao;
	@Override
	public List<MovieInfo> getMoviesInfo() {
		return dao.getMoviesInfo();
	}

	@Override
	public MovieInfo getMovieInfoByMovieName(Integer movieId) {
		return dao.getMovieInfoByMovieName(movieId);
	}

}

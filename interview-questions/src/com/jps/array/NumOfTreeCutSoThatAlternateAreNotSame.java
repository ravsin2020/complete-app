package com.jps.array;

/**
 * 
 * @author Ravi
 * 
 * Minimum number of tree cuts so that each pair of trees 
 * alternates between strictly decreasing and strictly increasing
 * 
Compilation successful.

Example test:   [3, 4, 5, 3, 7]
OK

Example test:   [1, 2, 3, 4]
OK

Example test:   [1, 3, 1, 2]
OK

Your test case: [1, 3, 1, 2]
Returned value: 0
 *
 */
public class NumOfTreeCutSoThatAlternateAreNotSame {

	public int solution(int[] A) {
		int val = -1;
		if (A == null || A.length < 3) {
			return val;
		}
		for (int i = 1; i < A.length - 1; i++) {
			if (A[i] > A[i - 1] && A[i] > A[i + 1]) {
				// val++;
				int k = i - 1;
				while (k > 0) {
					if (A[k] > A[i + 1]) {
						val++;
						k--;
					}
				}
			} else if (A[i] < A[i - 1] && A[i] < A[i + 1]) {
				// val++;
				int k = i - 1;
				while (k > 0) {
					if (A[k] > A[i]) {
						val++;
						k--;
					}
				}
			}
		}
		if (val > 1) {
			val++;
		}
		return val;

	}

}

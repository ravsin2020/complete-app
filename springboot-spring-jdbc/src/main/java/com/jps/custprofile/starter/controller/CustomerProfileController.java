package com.jps.custprofile.starter.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jps.custprofile.starter.beans.Customer;
import com.jps.custprofile.starter.service.CustomerService;

@RestController
@RequestMapping(path = "/customers")//, produces="application/json", consumes="application/json")
public class CustomerProfileController {
	private final static Logger logger=LoggerFactory.getLogger(CustomerProfileController.class);
	private static volatile Map<Integer, Customer> list= new HashMap();
	@Autowired
	CustomerService service;
	
	@GetMapping("/logger")
	public ResponseEntity<String> getLogger() {
		logger.error("I m Error");
		logger.warn("I am warn");
		logger.info("I am Info");
		logger.debug("Iam debug");
		logger.trace("I amTrace");
		
		return ResponseEntity.ok("I am Customer Logger");
	}
	
	//@RequestMapping(method = { RequestMethod.GET })
	@GetMapping
	public ResponseEntity<Map<Integer, Customer>> getCustomers() {
		logger.info("getCustomers - start");
		/*list.put(100,new Customer(100, "Ravi", 30));
		return list;*/
		Map<Integer, Customer> result = service.getCustomers();
		logger.debug("Customer data fetched");
		
		logger.info("getCustomers - Ends");
		return ResponseEntity.ok(result);
		
	}

	@RequestMapping(path = "/{id}", method = { RequestMethod.GET })
	public Customer getCustomerDetails(@PathVariable("id") Integer id) {
		Customer cust=null;
		/*if(list!=null && !list.isEmpty()){
			for(Map.Entry<Integer,Customer> cus:list.entrySet()){
				if(cus.getKey()==id){
					cust=cus.getValue();;
					break;
				}
			}
		}*/
		cust= service.getCustomerById(id);
		
		return cust;
	}
	
	@RequestMapping(method={RequestMethod.POST})
	public ResponseEntity createCustomer(@RequestBody Customer customer){
		//list.put(customer.getId(), customer);
		String result = service.createCustomer(customer);;
		return ResponseEntity.status(HttpStatus.CREATED).body(result);
	}
	
	@RequestMapping(method={RequestMethod.PUT})
	public ResponseEntity updateCustomer(@RequestBody Customer customer){
		//list.put(customer.getId(), customer);
		String result =  service.updateCustomer(customer);
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(method={RequestMethod.DELETE})
	public String deleteCustomer(@RequestBody Customer customer){
		
		return service.deleteCustomer(customer);
	}
}

package com.jps.moviecatalog.starter.bean;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

public class MovieCatalog implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 6957494636235444434L;
	private Customer customer;
	private List<MovieInfo> movies;
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public List<MovieInfo> getMovies() {
		return movies;
	}
	public void setMovies(List<MovieInfo> movies) {
		this.movies = movies;
	}
	

}

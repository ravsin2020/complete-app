package com.jps.ds;
/**
 * 
 * @author Ravikant.singh
 * Reverse Singly Linked List
 *
 */
public class ReverseSinglyLinkedList {
	public static void main(String[] args) {
		SiglyLinkedList<Integer> list =new SiglyLinkedList<>();
		list.add(10);
		list.add(20);
		list.add(30);
		list.iterate();
		System.out.println("After reverse");
		list.reverse();
		list.iterate();
		System.out.println("After reverse");
		list.reverseWithRec(null, list.getHead(), null);;
		list.iterate();
	}
private static class SiglyLinkedList<T>{
	private Node<T> head;
	SiglyLinkedList(){
		head=null;
	}
	public void reverse(){
		if(head!=null && head.next!=null){
			Node<T> prev=null;Node<T> current=head; Node<T> next;
			while(current!=null){
				next = current.next;
				current.next=prev;
				prev=current;
				current=next;
			}
			head=prev;
		}
	}
	public Node<T> reverseWithRec(Node<T> prev,Node<T> current, Node<T> next){
		if(current==null){
			head= prev;
			return head;
		}
		next = current.next;
		current.next=prev;
		prev = current;
		current = next;
		reverseWithRec(prev, current, next);	
		return head;
	}
	public void add(T data){
		Node<T> nextNode= new Node<>(data);
		if(head==null){
			head= nextNode;
		}else{
			Node<T> temp= head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=nextNode; 
		}
		
	}
	public void iterate(){
		if(head!=null){
			Node<T> temp= head;
			while(temp!=null){
				System.out.println(temp.data);
				temp=temp.next;
			}
		}
	}
	public Node<T> getHead() {
		return head;
	}
	
}
private static class Node<T>{
	private T data;
	private Node<T> next;
	Node(T data){
		this.data=data;
		this.next=null;
	}
}
}

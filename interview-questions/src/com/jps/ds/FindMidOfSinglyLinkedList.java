package com.jps.ds;
/**
 * 
 * @author Ravikant.singh
 * This class provides the functionality to find the middle of Singly Linked List  
 *
 */
public class FindMidOfSinglyLinkedList {
	public static void main(String[] args) {
		LinkedList<Integer> list = new LinkedList();
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);

		list.iterate();
		list.findMiddle();
	}

	private static class LinkedList<T> {
		Node<T> head;

		LinkedList() {
			this.head = null;
		}
		public void findMiddle(){
			Node<T> slow=head,fast=head;
			while(fast!=null && fast.next!=null){
				slow= slow.next;
				fast=fast.next.next;
			}
			System.out.println("Middle : " +slow.data );
			
		}

		public void add(T val) {
			Node<T> newNode = new Node<T>(val);
			if (head == null) {
				head = newNode;
			} else {
				Node<T> temp = head;
				while (temp.next != null) {
					temp = temp.next;
				}
				temp.next = newNode;
			}
		}

		public void iterate() {
			if (head != null) {
				Node<T> temp = head;
				while (temp != null) {
					System.out.println(temp.data);
					temp = temp.next;
				}
			}
		}
	}

	private static class Node<T> {
		private T data;
		private Node<T> next;

		Node(T data) {
			this.data = data;
			this.next = null;
		}
	}

}

package com.jps.test.FalabellaTest;

public class CharactorProgrammingAControllDevice {
	public static void main(String[] args) {
		System.out.println(getMaxDeletion(new StringBuilder("URRDR"), 0,1));
	}
	public static int getMaxDeletion(StringBuilder instructions, int start, int end) {
        if (end >= instructions.length() - 1) {
            return 0;
        }
        if ((instructions.charAt(start) + instructions.charAt(end) == 'U' + 'D') || (instructions.charAt(start) + instructions.charAt(end) == 'L' + 'R')) {
            System.out.println("Deleted:\n" + start + "-" + instructions.charAt(start) + "\n" + end + "-" + instructions.charAt(end));
            instructions.deleteCharAt(start);
            instructions.deleteCharAt(end);

            return 2;
        }

        int delCount = 0;
        delCount += getMaxDeletion(instructions, start + 1, start + 2);
        delCount += getMaxDeletion(instructions, start, start + 2);
        return delCount;
    }
}

package com.jps.starter.controller.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jps.starter.bean.Customer;

public interface CustomerService {
	public List<Customer> getAllCustomer();

	public void createCustomer(Customer customer);

}

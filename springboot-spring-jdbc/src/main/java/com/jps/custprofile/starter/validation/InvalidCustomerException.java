package com.jps.custprofile.starter.validation;

public class InvalidCustomerException extends RuntimeException{
	private String msg;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public InvalidCustomerException(){
		
	}

	public InvalidCustomerException(String msg){
		this.msg=msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}

package com.jps.ticketbooking.starter.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jps.ticketbooking.starter.bean.Ticket;

@Repository
public class TicketBookingDAOImpl implements TicketBookingDAO {
	@Autowired
	private SessionFactory factory;
	
	@Override
	public List<Ticket> getTickets() {
		List<Ticket> tickets= factory.getCurrentSession().createCriteria(Ticket.class).list();
		return tickets;
	}

	@Override
	public List<Ticket> getTicketsByCustomerId(Integer custId) {
		Session session= factory.getCurrentSession();
		Query q= session.createQuery("from tickets where custId=:custId");
		q.setParameter("custId", custId);
		List<Ticket> tickets = q.list();
		return tickets;
	}

}

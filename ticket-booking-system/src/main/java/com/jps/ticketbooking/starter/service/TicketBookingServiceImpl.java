package com.jps.ticketbooking.starter.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jps.ticketbooking.starter.bean.Ticket;
import com.jps.ticketbooking.starter.dao.TicketBookingDAO;
@Service
@Transactional
public class TicketBookingServiceImpl implements TicketBookingService{
	@Autowired
	TicketBookingDAO bookingDao;

	@Override
	public List<Ticket> getTickets() {
		return bookingDao.getTickets();
	}

	@Override
	public List<Ticket> getTicketsByCustomerId(Integer custId) {
		return bookingDao.getTicketsByCustomerId(custId);
	}

}

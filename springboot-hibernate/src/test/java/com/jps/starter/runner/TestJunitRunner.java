package com.jps.starter.runner;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import com.jps.starter.controller.CustomerRestControllerTest;

public class TestJunitRunner {
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(CustomerRestControllerTest.class);

		for(
		Failure failure:result.getFailures())
		{
			System.out.println(failure.toString());
		}

		System.out.println(result.wasSuccessful());
	}
}

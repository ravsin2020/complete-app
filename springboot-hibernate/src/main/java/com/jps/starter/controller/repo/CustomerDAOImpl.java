package com.jps.starter.controller.repo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jps.starter.bean.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO{
	@Autowired
	private SessionFactory sessionFactory;
 
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	@Override
	public List<Customer> getAllCustomer() {
		int start = 0; int pageSize=10;
		Session session = this.sessionFactory.getCurrentSession();
		//Query  query = session.createQuery("from Customer");
		Criteria criteria= session.createCriteria(Customer.class);
		//pagination using Criteria. we can use the same way for Query as well
		criteria.setFirstResult(start);
		criteria.setMaxResults(pageSize);
		List<Customer> customerList =  criteria.list();
		return customerList;
	}
	@Override
	public void createCustomer(Customer customer) {
		Session session=this.sessionFactory.getCurrentSession();
		session.save(customer);
	}
	
}

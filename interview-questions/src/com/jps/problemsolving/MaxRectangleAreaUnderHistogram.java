package com.jps.problemsolving;

import java.util.Stack;

/**
 * 
 * @author Ravikant.singh
 *
 *         We can do using 3 ways 1. Using Brute Force - then time complexity
 *         will be o(n)2 2. Using Divide and conquer - then time complexity will
 *         o(n log n) 3. Using Stack - this can be done in O(n) The below
 *         program is using using Stack only
 *         **
 *         width of all bars is assumed to be 1 for simplicity. For every bar �x�, we calculate the area with �x� as the smallest bar in the rectangle. If we calculate such area for every bar �x� and find the maximum of all areas, our task is done. How to calculate area with �x� as smallest bar? We need to know index of the first smaller (smaller than �x�) bar on left of �x� and index of first smaller bar on right of �x�. Let us call these indexes as �left index� and �right index� respectively.
We traverse all bars from left to right, maintain a stack of bars. Every bar is pushed to stack once. A bar is popped from stack when a bar of smaller height is seen. When a bar is popped, we calculate the area with the popped bar as smallest bar. How do we get left and right indexes of the popped bar � the current index tells us the �right index� and index of previous item in stack is the �left index�. Following is the complete algorithm.

1) Create an empty stack.

2) Start from first bar, and do following for every bar �hist[i]� where �i� varies from 0 to n-1.
��a) If stack is empty or hist[i] is higher than the bar at top of stack, then push �i� to stack.
��b) If this bar is smaller than the top of stack, then keep removing the top of stack while top of the stack is greater. Let the removed bar be hist[tp]. Calculate area of rectangle with hist[tp] as smallest bar. For hist[tp], the �left index� is previous (previous to tp) item in stack and �right index� is �i� (current index).

3) If the stack is not empty, then one by one remove all bars from stack and do step 2.b for every removed bar.
 */
public class MaxRectangleAreaUnderHistogram {
	public static void main(String[] args) {
		int[] hist = { 6,2,2,5,4,5,1,6 };
		System.out.println("Max area of hist"+finMaxAreaHist(hist));
		int[] hist2 = { 6,2,2,5,4,5,1,6 };
		System.out.println(finMaxAreaHist(hist2));
	}

	public static int finMaxAreaHist(int[] arr) {
		if (arr == null || arr.length == 0) {
			return 0;
		}
		int maxArea = 0, area = 0;
		int i = 0;
		int end = arr.length;
		int top;
		Stack<Integer> s = new Stack<>();
		while (i < end) {
			if (s.isEmpty() || arr[i] >= arr[s.peek()]) {
				s.push(i++);
				i++;
			} else {
				top = s.pop();
				// area = currentVal *(right Max);
				// area= current Value * (right max -leftMax-1) -- if stack is
				// not empty
				area = arr[top] * (s.isEmpty() ? i : (i - s.peek() - 1));
				if (area > maxArea) {
					maxArea = area;
				}
			}
		}
		if (!s.isEmpty()) {
			top = s.pop();
			area = arr[top] * (s.isEmpty() ? i : (i - s.peek() - 1));
			if (area > maxArea) {
				maxArea = area;
			}
			i++;
		}
		return maxArea;
	}
}
